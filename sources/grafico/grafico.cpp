#include "grafico.h"
#include<iostream>

using namespace std;

/**
 *  \brief Desenha objeto na tela
 *  
 *  \param [in] obj Objeto a ser desenhado na tela
 *  \param [in] hover Indica se é passivel de mudança com o mouse
 *  
 */
void printObject(object_data_type *obj,bool hover)
{
	int mode = COPY_PUT;
	int x, y, img_y = obj->pos.y;

	if(hover)
	{
		POINT mouse_pos;
		
		if(GetCursorPos(&mouse_pos))
		{
			HWND hwnd = GetForegroundWindow(); 
			if(ScreenToClient(hwnd, &mouse_pos))
			{
			
				x = mouse_pos.x;
				y = mouse_pos.y;
		
				if((x > obj->pos.x) && (x < (obj->pos.x + obj->image.w)) && (y > obj->pos.y) && (y < (obj->pos.y + obj->image.h)))
					img_y -= 5;
			}
		}
	}

	if(obj->image.msk)
	{
	 	putimage(obj->pos.x, img_y, obj->image.msk, AND_PUT);
	 	mode = OR_PUT;
	 }
	
	putimage(obj->pos.x, img_y, obj->image.img, mode);
}

/**
 *  \brief Carrega na memoria os objetos para serem desenhados posteriormente
 *  
 *  \param [in] objeto Dados da imagem (h,w e ponteiro para memoria)
 *  \param [in] caminho Caminho do arquivo da imagem
 *  \param [in] caminhomsk Caminho do arquivo da mascara
 *  
 */
void graphInitObject(img_data_type *objeto, const char* caminho, const char* caminhomsk){
	unsigned size;
	int left, top, right, bottom;
	//setbkcolor(BGCOLOR);
	
	int page = getactivepage();
	setactivepage(2);
	
	cleardevice();
	left = 0;
	top = 0;
	right = objeto->w;
	bottom = objeto->h;
	
	readimagefile(caminho,left, top, right, bottom);
	size= imagesize(left, top,right,bottom);
	objeto->img = new int[size];
	getimage(left, top, right, bottom, objeto->img);

	cleardevice();
	
	if(strcmp(caminhomsk," ")){
		readimagefile(caminhomsk,left, top, right, bottom );
		size= imagesize(left, top,right,bottom);
		objeto->msk = new int[size];
		getimage(left, top, right, bottom, objeto->msk);
		cleardevice();	
	}
	
	setactivepage(page);
}

void desenhaNivel(object_data_type *obj, int nivel, bool pureza)
{
	setcolor(WHITE);
	
	if(pureza)
		setfillstyle(SOLID_FILL,LIGHTBLUE);
	else
		setfillstyle(SOLID_FILL,BROWN);
	
	if(obj->pos.x)
	{
		rectangle(obj->pos.x-25,obj->pos.y+obj->image.h-140,obj->pos.x-5,obj->pos.y+obj->image.h-40);
		bar(obj->pos.x-25,obj->pos.y+obj->image.h-nivel-40,obj->pos.x-5,obj->pos.y+obj->image.h-40);
	}
	else
	{
		rectangle(obj->pos.x+obj->image.w+5,obj->pos.y+obj->image.h-140,obj->pos.x+obj->image.w+25,obj->pos.y+obj->image.h-40);
		bar(obj->pos.x+obj->image.w+5,obj->pos.y+obj->image.h-nivel-40,obj->pos.x+obj->image.w+25,obj->pos.y+obj->image.h-40);
	}
}

bool collidingObject(object_data_type *obj1,object_data_type *obj2)
{
	if(obj1->pos.x > obj2->pos.x+obj2->image.w || obj1->pos.x+obj1->image.w < obj2->pos.x)
		return false;
		
	if(obj1->pos.y > obj2->pos.y+obj2->image.h || obj1->pos.y+obj1->image.h < obj2->pos.y)
		return false;
		
	return true;	
}

// Atualiza a tela com o que foi desenhado
void updateScreen(){
	int page = getactivepage();
	setvisualpage(page);
	
	page = page ? 0:1;
	setactivepage(page);
	cleardevice();
}
