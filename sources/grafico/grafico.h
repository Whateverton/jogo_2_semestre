#include "../../resources/graphics.h"
#include "../../resources/types.h"

#ifndef __GRAFICO_H__
#define __GRAFICO_H__

#define SCREEN_W 1280
#define SCREEN_H 720
#define BG_PATH "../resources/images/background/1280x720_f.jpg"


#define SCENE_PATH "../resources/images/background/cenario_BUCK.jpg"
#define DARK_PATH "../resources/images/background/escurece.jpg"
#define BUCKY_PATH "../resources/images/bucky/bucky_menu01.jpg"
#define BUCKY_MASK "../resources/images/bucky/bucky_menu01_MASK.jpg"

void printObject(object_data_type *obj, bool hover = false);
void graphInitObject(img_data_type *objeto, const char* caminho, const char* caminhomsk = " ");
bool collidingObject(object_data_type *obj1,object_data_type *obj2);
void desenhaNivel(object_data_type *obj, int nivel, bool pureza);

void updateScreen();

#endif
