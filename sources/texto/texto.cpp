/**
 * @brief Arquivo contendo os textos nos 2 idiomas (portugues, ingl�s)
 *
 * @date 21/03/15
 * @author Alexandre, Claudio, Everton, Tercio e Wallace
 * @version 1.0
 */
 #define QTD_IDIOMA 2
 
const char *single_player[QTD_IDIOMA] = {"Um Jogador","Single Player"};
const char *multi_player[QTD_IDIOMA] = {"Dois Jogadores","Multi Player"};
const char *settings[QTD_IDIOMA] = {"Configura��es","Settings"};
const char *back_button[QTD_IDIOMA] = {"Voltar", "Back"};
const char *exit_game[QTD_IDIOMA] = {"Sa�da","Exit"};

const char *lets_play[QTD_IDIOMA] = {"Vamos jogar!","Let's play!"};

const char *language[QTD_IDIOMA] = {"_port", "_eng"};
