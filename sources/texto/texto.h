/**
 * @brief Arquivo contendo os textos nos 2 idiomas (portugues, ingl�s)
 *
 * @date 21/03/15
 * @author Alexandre, Claudio, Everton, Tercio e Wallace
 * @version 1.0
 */
#ifndef __TEXTO_H__
#define __TEXTO_H__

extern const char *single_player[QTD_IDIOMA];
extern const char *multi_player[QTD_IDIOMA];
extern const char *settings[QTD_IDIOMA];
extern const char *back_button[QTD_IDIOMA];
extern const char *exit_game[QTD_IDIOMA];

extern const char *lets_play[QTD_IDIOMA];

extern const char *language[QTD_IDIOMA];
#endif
