#ifndef __WATER_GAME_H__
#define __WATER_GAME_H__

#define DATA_PATH	"../resources/data/graph_data.cvs"
#define LANGUAGE_PATH	"../resources/data/language.txt"
#define SERVER_PATH	"../resources/data/server.inf"

game_state_type loadGame();
game_state_type showdMainMennu();
game_state_type showConfigMenu();
game_state_type multiPlayer();
game_state_type singlePlayer();

#endif