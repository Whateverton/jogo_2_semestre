/**
 * @brief Arquivo main para inicializa��es e loop principal do jogo
 *
 * @date 18/03/15
 * @author Alexandre, Claudio, Everton, Tercio e Wallace
 * @version 1.0
 */
#include<iostream>
#include <time.h>
#include "grafico/grafico.h"
#include "texto/texto.h"
#include "water_game.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv)
{
	game_state_type game_state;
	static char func_update = 4;


	// Inicia no esstado INIT, der
	game_state = INIT;
	
	srand(time(NULL));
	
	// Inicializa a janela com a resolucao definida
	initwindow(SCREEN_W,SCREEN_H,"Bucky, The Bucket");
		
	for(;;) //while(true)
	{		
		switch(game_state)
		{
			case INIT:				
				game_state = loadGame(); // passar parametro para retornar o mesmo, pra voltar de onde saiu
			break;
			
			case MENU:
				game_state = showdMainMennu();
			break;
			
			case CONFIG:
				game_state = showConfigMenu();
			break;
			
			case SINGLE:
				game_state = singlePlayer();
			break;
			
			case MULTI:
				game_state = multiPlayer();
			break;
			
			case CREDITS:
			break;
			
			case EXIT:
				return 0;
			
			default:
			break;
		}

		// Atualiza todos objetos em cena
		if(func_update) --func_update;
		else 
		{
			updateScreen();
			
			// D� o tempo entre execu��es, equivalente a 60fps
			delay(((1/240.0) * 1000.0));
			
			func_update = 4;
		}
	}
	
	return 0;
}
