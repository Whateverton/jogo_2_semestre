#include <windows.h>
#include <winsock.h>
#include <stdio.h>
#include <iostream>

#ifndef __REDE_H__
#define __REDE_H__

#define SOCK_BUFF_LEN 64

enum{
	CONNECTION_REQ,
	SENDING_DATA,

	PLAYER_STATUS,
	TANK_UPDATE,
	CLOUD_UPDATE,
	RAIN_UPDATE,
	BIRD_UPDATE,
	SHIT_UPDATE,
	DROP_UPDATE,
	TIME_UPDATE
};

typedef struct
{
	unsigned int operation;
	unsigned int buff_size;
	char buff[SOCK_BUFF_LEN];
}data_type;

// Prototipos -------------------------------------------------------
void configServer (char *ip, int port);
bool iniRede();
bool connectToServer();
bool waitClient(void);
bool getData(data_type &pack);
bool sendData(data_type &p);
// ------------------------------------------------------------------
#endif
