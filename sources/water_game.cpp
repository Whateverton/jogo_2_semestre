/**
 * @brief Funcoes principais do jogo
 *
 * @date 28/03/15
 * @author Alexandre, Claudio, Everton, Tercio e Wallace
 * @version 1.0
 */
#include "grafico/grafico.h"
#include "texto/texto.h"
#include "water_game.h"
#include "rede/rede.h"
#include <fstream>

using namespace std;

#define PACE 25
#define VALUE_COUNT 3000
#define TIME_FOSSET 750
#define TIME_CHUVA 125
#define TIME_BIRD 250

void buckyDance();
void resetGame();
bool contador();
in_game_type runClient();
in_game_type runServer();

idioma_type current_language = PORT;

img_data_type game_images[QTD_GAME_OBJ];
img_data_type bg_img;
img_data_type scene_img;
img_data_type escurece_img;
img_data_type bucky_img;

object_data_type main_menu[QTD_MAIN_MENU];
object_data_type config_menu[QTD_CONFIG_MENU];
object_data_type baloes_menu[QTD_BALOES];
object_data_type venceu;
object_data_type perdeu;
object_data_type mult_menu[5];
object_data_type pause_menu[3];
object_data_type lose_menu;
object_data_type torneira;
object_data_type nuvem;
object_data_type drop;
object_data_type building;
object_data_type water_tank;
object_data_type pombo;
object_data_type detrito;
object_data_type chuva;
object_data_type player_1[2+14];
object_data_type player_2[2];
object_data_type background;
object_data_type scene_object;
object_data_type buckyground;
object_data_type escurece;

int drop_start, rain_start, shit_start;
int contador_value = VALUE_COUNT;

// Carrega objetos do jogo e afins
game_state_type loadGame()
{
	char data[8000], language_data[10], *pointer;
	int i, file_index = 0;
	static char path_img[200], path_mask[200], width[4], height[5],format[6];

	ifstream data_file(DATA_PATH);

	// Le de um arquivo os dados das imagens
	while(data_file.good())
	{
		data[file_index] = data_file.get();
		file_index++;
	}

	data_file.close();
	data[file_index] = '\0';
	
	file_index = 0;
	ifstream language_file(LANGUAGE_PATH);

	// Le de um arquivo qual idioma salvo
	while(language_file.good())
	{
		language_data[file_index] = language_file.get();
		file_index++;
	}

	language_file.close();
	
	if(!file_index)
		current_language = PORT;
	else
	{
		language_data[file_index] = '\0';
		
		// Seta idioma, fazer ler de arquivo depois!
		
		pointer = strtok(language_data,"\n\0");
		current_language = (idioma_type)atoi(pointer);
	}
	
	// carrega formator das imagens
	pointer = strtok(data,"\n\0");
	strcpy(format,pointer);
		
	// Carrrega todas imagens do jogo
	for(i = 0; i < QTD_GAME_OBJ; ++i)
	{
		if(current_language == ING)
		{
			pointer = strtok(NULL,",");
			pointer = strtok(NULL,",");
		}
		
		// Carrega o valor da largura
		pointer = strtok(NULL,",");
		
		strcpy(width,pointer);
		game_images[i].w = atoi(width);
		
		// Carrega o valor da altura
		pointer = strtok(NULL,",");
		strcpy(height,pointer);
		game_images[i].h = atoi(height);
		
		if(current_language == PORT)
		{
			pointer = strtok(NULL,",");
			pointer = strtok(NULL,",");
		}
		
		// Carrega a mascara
		pointer = strtok(NULL,"\n\0");
		strcpy(path_mask,pointer);
		
		// Completa o nome do arquivo da mascara
		strcat(path_mask,language[current_language]);
		strcat(path_mask,"_MASK");
		strcat(path_mask,format);
		
		// Carrega a imagem
		strcpy(path_img,pointer);
		
		// Completa o nome do arquivo da imagem
		strcat(path_img,language[current_language]);
		strcat(path_img,format);
		
		graphInitObject(&game_images[i],path_img,path_mask);
	}

	bg_img.w = SCREEN_W;	
	bg_img.h = SCREEN_H;
	graphInitObject(&bg_img,BG_PATH);

	background.image = bg_img;
	background.pos.x = 0;
	background.pos.y = 0;
	
	escurece_img.w = SCREEN_W;	
	escurece_img.h = SCREEN_H;
	graphInitObject(&escurece_img," ",DARK_PATH);

	escurece.image = escurece_img;
	escurece.pos.x = 0;
	escurece.pos.y = 0;
	
	scene_img.w = SCREEN_W;	
	scene_img.h = SCREEN_H;
	graphInitObject(&scene_img,SCENE_PATH);

	scene_object.image = scene_img;
	scene_object.pos.x = 0;
	scene_object.pos.y = 0;

	bucky_img.w = 507;
	bucky_img.h = 391;
	graphInitObject(&bucky_img,BUCKY_PATH,BUCKY_MASK);

	buckyground.image = bucky_img;
	buckyground.pos.x = SCREEN_W-600;
	buckyground.pos.y = SCREEN_H-400;
	
	for(i = 0; i < 16; ++i)
	{
		player_1[i].image = game_images[PLAYER1+i];
		player_1[i].pos.x = 0;
		player_1[i].pos.y = SCREEN_H-player_1[i].image.h;
	}
	
	for(i = 0; i < 2; ++i)
	{		
		player_2[i].image = game_images[PLAYER2+i];
		player_2[i].pos.x = 0;
		player_2[i].pos.y = SCREEN_H-player_2[i].image.h;
		
	}
	
	building.image = game_images[BUILDING];
	building.pos.y = SCREEN_H-game_images[BUILDING].h-80;
	building.pos.x = SCREEN_W-game_images[BUILDING].w;
	
	water_tank.image = game_images[WATER_TANK];
	water_tank.pos.y = SCREEN_H-game_images[WATER_TANK].h-game_images[BUILDING].h-40;
	water_tank.pos.x = SCREEN_W-game_images[WATER_TANK].w;
	
	torneira.image = game_images[FOSSET];
	torneira.pos.y = SCREEN_H-180;
	torneira.pos.x = SCREEN_W-game_images[BUILDING].w;
	
	drop.image = game_images[DROP];
	drop.pos.y = drop_start = torneira.pos.y+game_images[DROP].h+20;
	drop.pos.x = torneira.pos.x;
	
	nuvem.image = game_images[CLOUD];
	nuvem.pos.y = 40;
	nuvem.pos.x = 0;
	
	pombo.image = game_images[BIRD];
	pombo.pos.y = 40;
	pombo.pos.x = 0;
	
	detrito.image = game_images[SHIT];
	detrito.pos.y = shit_start = pombo.pos.y+game_images[SHIT].h;
	detrito.pos.x = 0;
	
	chuva.image = game_images[WATER];
	chuva.pos.y = rain_start = nuvem.pos.y+game_images[WATER].h;
	chuva.pos.x = 0;
	
	venceu.image = game_images[GANHOU];
	venceu.pos.y = SCREEN_H/2 - venceu.image.h/2;
	venceu.pos.x = SCREEN_W/2 - venceu.image.w/2;
	
	perdeu.image = game_images[PERDEU];
	perdeu.pos.y = SCREEN_H/2 - perdeu.image.h/2;
	perdeu.pos.x = SCREEN_W/2 - perdeu.image.w/2;
	
	for(i = 0; i < 2; ++i)
	{
		mult_menu[i].image = game_images[CREATE + i];
		mult_menu[i].pos.y = 20 + i*80;
		mult_menu[i].pos.x = 20;
	}
	
	mult_menu[2].image = game_images[BACK];
	mult_menu[2].pos.y = 180;
	mult_menu[2].pos.x = 20;
	
	lose_menu.image = game_images[BACK];
	lose_menu.pos.y = SCREEN_H - lose_menu.image.h;
	lose_menu.pos.x = SCREEN_W/2 - lose_menu.image.w/2;
	
	for(i = 3; i < 5; ++i)
	{
		mult_menu[i].image = game_images[QUIT_GAME + i];
		mult_menu[i].pos.y = SCREEN_H/2 - mult_menu[i].image.h/2;
		mult_menu[i].pos.x = SCREEN_W/2 - mult_menu[i].image.w/2;
	}
	
	for(i = 0; i < QTD_BALOES; ++i)
	{
		baloes_menu[i].image = game_images[BALAO1 + i];
		baloes_menu[i].pos.y = SCREEN_H-700;
		baloes_menu[i].pos.x = SCREEN_W-500;
	}
	
	for(i = 0; i < QTD_MAIN_MENU; ++i)
	{
		main_menu[i].image = game_images[SINGLEPLAYER + i];
		main_menu[i].pos.y = 20 + i*80;
		main_menu[i].pos.x = 20;
	}

	for(i = 0; i < QTD_CONFIG_MENU; ++i)
	{
		config_menu[i].image = game_images[LANGUAGE + i];
		config_menu[i].pos.y = 20 + i*80;
		config_menu[i].pos.x = 20;
	}
	
	for(i = 0; i < 3; ++i)
	{
		pause_menu[i].image = game_images[PAUSE + i];
		pause_menu[i].pos.y = i ? 250 + i*80: 20;
		pause_menu[i].pos.x = SCREEN_W/2 - game_images[PAUSE + i].w/2;
	}
	
	
	ifstream server_file(SERVER_PATH);

	file_index = 0;
	
	// Le de um arquivo os dados das imagens
	while(server_file.good())
	{
		data[file_index] = server_file.get();
		file_index++;
	}

	server_file.close();
	data[file_index] = '\0';
	
	char *ip = strtok(data,":");
	pointer = strtok(NULL,":");
	int port = strtoul(pointer,0,10);
	
	configServer(ip,port);
	
	PlaySound("..\\resources\\sounds\\rain_2.wav",NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);
	
	return MENU;
}

// Estado de exibição do menu
game_state_type showdMainMennu()
{
	int i,x,y;

	printObject(&background);
	printObject(&buckyground);
	printObject(&baloes_menu[0]);

	for(i = 0; i < QTD_MAIN_MENU; ++i)
		printObject(&main_menu[i],true);
	
	if(ismouseclick(WM_LBUTTONDOWN))
	{
		getmouseclick(WM_LBUTTONDOWN,x,y);
			
		for(i = 0; i < QTD_MAIN_MENU; ++i)
		{
			if((x > main_menu[i].pos.x) && (x < (main_menu[i].pos.x + main_menu[i].image.w)) && (y > main_menu[i].pos.y) && (y < (main_menu[i].pos.y + main_menu[i].image.h)))
			{
				switch(i)
				{
					case 0:	return SINGLE;
					
					case 1:	return MULTI;
					
					case 2:	return CONFIG;
					
					case 3:	return EXIT;
				}
			}
		}
	}
	return MENU;
}

// Estado da exibição das configurações
game_state_type showConfigMenu()
{
	int i,x,y;
	char ch[2];
	
	printObject(&background);
	printObject(&buckyground);

	for(i = 0; i < QTD_CONFIG_MENU; ++i)
		printObject(&config_menu[i],true);
	
	if(ismouseclick(WM_LBUTTONDOWN))
	{
		getmouseclick(WM_LBUTTONDOWN,x,y);
			
		for(i = 0; i < QTD_MAIN_MENU; ++i)
		{
			if((x > config_menu[i].pos.x) && (x < (config_menu[i].pos.x + config_menu[i].image.w)) && (y > config_menu[i].pos.y) && (y < (config_menu[i].pos.y + config_menu[i].image.h)))
			{
				switch(i)
				{
					case 0:
						{
							ofstream outfile(LANGUAGE_PATH);
							current_language = current_language ? PORT : ING;
							itoa((int)current_language,ch,10);
							ch[1] = '\0';
							outfile.write(ch,2);
							
							outfile.close();

							loadGame();
						}
					break;
					
					case 1:	return MENU;
				}
			}
		}
	}
	return CONFIG;
}

game_state_type singlePlayer()
{
	static in_game_type single_player_state = INTRO;
	static int intro = SCREEN_W/2;
	static char nivel_balde = 0, nivel_caixa = 0, image_player = 0;
	static bool agua_limpa = true, nuvem_sentido = true, pegou_chuva = false, poop = false;
	static int count_torneira = TIME_FOSSET, count_chuva = TIME_CHUVA, count_pombo = TIME_BIRD;
	static char roda_func = 0;
	static bool dance = true;
	int x,y;

	if(roda_func)
	{
		--roda_func;
		return SINGLE;
	}
	else
	{
		roda_func = 4;
	}
	
	// Desenha toda cena
	printObject(&scene_object);
	printObject(&building);
	printObject(&torneira);
	printObject(&nuvem);
	printObject(&water_tank);
	printObject(&player_1[image_player]);
	desenhaNivel(&player_1[image_player],nivel_balde,agua_limpa);
	desenhaNivel(&water_tank,nivel_caixa,true);
	
	switch(single_player_state)
	{
		case INTRO:
		{
			char contagem[2];
			
			intro -= SCREEN_H/60;
			setcolor(LIGHTGREEN);
			setfillstyle(INTERLEAVE_FILL,LIGHTGREEN);
			fillellipse(SCREEN_W/2,SCREEN_H/2,intro,intro);
			
			// if(intro%5)
			// {
				// setcolor(RED);
				// setbkcolor(YELLOW);
				// settextstyle(4,HORIZ_DIR, 40);
				// itoa(intro/60,contagem,10);
				// outtextxy(SCREEN_W/2-20,SCREEN_H/2,contagem);
			// }
				
			if(intro <= 0)
			{
				// setcolor(BLUE);
				// setbkcolor(WHITE);
				// settextstyle(4,HORIZ_DIR, 40);
				// outtextxy(SCREEN_W/2-100,SCREEN_H/2,(char*)lets_play[current_language]);
				// updateScreen();
				// delay(700);
			
				single_player_state = PLAYING;
			}

			return SINGLE;
		}
		break;
		
		case PLAYING:
			// Faz movimentos			
			
			if((player_1[image_player].pos.y+player_1[image_player].image.h) >= SCREEN_H)
			{
				if(image_player)
				{
					image_player = 0;
					
					player_1[0].pos.x = player_1[1].pos.x;
					player_1[0].pos.y = player_1[1].pos.y;
				}
				
				
				if(GetKeyState(VK_LEFT)&0x80) player_1[image_player].pos.x -= PACE - (nivel_balde/10);
				if(GetKeyState(VK_RIGHT)&0x80) player_1[image_player].pos.x += PACE - (nivel_balde/10);
			}
			else if(!image_player)
			{
				image_player = 1;
				
				player_1[1].pos.x = player_1[0].pos.x;
				player_1[1].pos.y = player_1[0].pos.y;
			}
			
			
			if(collidingObject(&player_1[image_player],&torneira))
				if(GetKeyState('X')&0x80)
					if(!count_torneira)
						count_torneira = TIME_FOSSET;
			
			if((player_1[image_player].pos.x+player_1[image_player].image.w) == SCREEN_W)
			{
				if(GetKeyState(VK_UP)&0x80){player_1[image_player].pos.y -= PACE - 5 - (nivel_balde/10);}
				if(GetKeyState(VK_DOWN)&0x80){player_1[image_player].pos.y += PACE - 5 - (nivel_balde/10);}
				
					if(nivel_balde)
						if((player_1[image_player].pos.y+player_1[image_player].image.h) == 270)
							if(GetKeyState(VK_SPACE)&0x80)
							{
								if(agua_limpa)
								{
									if(nivel_balde > 0) nivel_balde -= 10;
									else nivel_balde = 0;
									
									nivel_caixa += 3;
									
									if(nivel_caixa > 100) nivel_caixa = 100;
								}
								else single_player_state = ENDING;
								
							}
			}
			
			
			if(!agua_limpa)
			{
				if(GetKeyState(VK_SPACE)&0x80)
				{
					if(nivel_balde > 0) nivel_balde -= 10;
					else nivel_balde = 0;
				}
				if(!nivel_balde) agua_limpa = true;
			}

			// Controla limites
			if(player_1[image_player].pos.x < 0) player_1[image_player].pos.x = 0;
			if((player_1[image_player].pos.x+player_1[image_player].image.w) >= SCREEN_W) player_1[image_player].pos.x = SCREEN_W-player_1[image_player].image.w;
			if((player_1[image_player].pos.y+player_1[image_player].image.h) >= SCREEN_H) player_1[image_player].pos.y = SCREEN_H-player_1[image_player].image.h;
			if((player_1[image_player].pos.y+player_1[image_player].image.h) <= 270) player_1[image_player].pos.y = 270-player_1[image_player].image.h;
			
			if(nuvem_sentido)
				nuvem.pos.x += PACE;
			else
				nuvem.pos.x -= PACE;

			if(nuvem.pos.x < 0)
			{
				nuvem.pos.x = 0;
				nuvem_sentido = true;
			}
			
			if((nuvem.pos.x+nuvem.image.w) >= SCREEN_W)
			{
				nuvem.pos.x = SCREEN_W-nuvem.image.w;
				nuvem_sentido = false;
			}
			
			if(count_pombo) --count_pombo;
			
			if(!count_pombo)
			{
				printObject(&pombo);
				
				if(!poop)
					if(collidingObject(&pombo,&nuvem))
					{
						detrito.pos.x = pombo.pos.x;
						poop = true;
					}

				pombo.pos.x += PACE;

				if(pombo.pos.x >= SCREEN_W)
				{
					pombo.pos.x = 0;
					count_pombo = TIME_BIRD;
				}
			}
			
			if(poop)
			{
				printObject(&detrito);
				
				if(collidingObject(&detrito,&player_1[image_player])) agua_limpa = false;
					
				if(detrito.pos.y >= SCREEN_H)
				{
					detrito.pos.y = shit_start;
					poop = false;
				}
				else
					detrito.pos.y += 20;
			}
			
			if(nivel_caixa)
				if(count_torneira)
					--count_torneira;
			
			if(!count_torneira)
			{
				if(nivel_caixa)
				{
					printObject(&drop);

					if(drop.pos.y+drop.image.h >= SCREEN_H)
					{
						drop.pos.y = drop_start;
						nivel_caixa -= 1;
					}
					else
						drop.pos.y += 5;
				}
			}
			
			if(count_chuva)
			{
				--count_chuva;
				chuva.pos.x = nuvem.pos.x;
			}
			else
			{
				if(!pegou_chuva)
					if(collidingObject(&player_1[image_player],&chuva))
					{
						pegou_chuva = true;
					}

				if((chuva.pos.y >= SCREEN_H) || collidingObject(&player_1[image_player],&chuva))
				{	
					if(collidingObject(&player_1[image_player],&chuva))
						if(nivel_balde < 100) nivel_balde += 25;
						else nivel_balde = 100;
					
					chuva.pos.y = rain_start;
					count_chuva = TIME_CHUVA;
				}
				else
				{
					printObject(&chuva);
					chuva.pos.y += 10;					
				}
			}
			
			if(GetKeyState(VK_ESCAPE)&0x80)
			{
				intro = SCREEN_W/2;
				//return MENU;
				single_player_state = PAUSED;
			}
			
			if(nivel_caixa == 100)
			{
				single_player_state = ENDING;
			}
			
			if(contador()) single_player_state = ENDING;
		break;
		
		case PAUSED:
			printObject(&escurece);
			
			printObject(&pause_menu[0]);
			printObject(&pause_menu[1],true);
			printObject(&pause_menu[2],true);
			
			if(ismouseclick(WM_LBUTTONDOWN))
			{
				int x,y,i;
				getmouseclick(WM_LBUTTONDOWN,x,y);
					
				for(i = 1; i < 3; ++i)
				{
					if((x > pause_menu[i].pos.x) && (x < (pause_menu[i].pos.x + pause_menu[i].image.w)) && (y > pause_menu[i].pos.y) && (y < (pause_menu[i].pos.y + pause_menu[i].image.h)))
					{
						switch(i)
						{
							case 1:
								single_player_state = PLAYING;
							break;
							
							case 2:
								single_player_state = INTRO;
								return MENU;
							break;
						}
					}
				}
			}
			
		break;
		
		case ENDING:
			if(dance)
			{
				buckyDance();
				dance = false;
			}
		
			printObject(&escurece);
			
			if(nivel_caixa == 100)
				printObject(&venceu);
			else
				printObject(&perdeu);
			
			printObject(&lose_menu,true);
			
			if(ismouseclick(WM_LBUTTONDOWN))
			{
				getmouseclick(WM_LBUTTONDOWN,x,y);

				if((x > lose_menu.pos.x) && (x < (lose_menu.pos.x + lose_menu.image.w)) && (y > lose_menu.pos.y) && (y < (lose_menu.pos.y + lose_menu.image.h)))
				{
					dance = true;
					resetGame();
					single_player_state = INTRO;
					nivel_balde = 0;
					nivel_caixa = 0;
					image_player = 0;
					agua_limpa = true;
					nuvem_sentido = true;
					pegou_chuva = false;
					poop = false;
					count_torneira = TIME_FOSSET;
					count_chuva = TIME_CHUVA;
					count_pombo = TIME_BIRD;
					roda_func = 0;
					return MENU;
				}
			}
		break;
	}

	return SINGLE;
}


game_state_type multiPlayer()
{
	static in_game_type multi_player_state = INTRO;
	int i,x,y;

	switch(multi_player_state)
	{
		case INTRO:
		
			printObject(&background);
			printObject(&buckyground);

			for(i = 0; i < 3; ++i)
				printObject(&mult_menu[i],true);
			
			if(ismouseclick(WM_LBUTTONDOWN))
			{
				getmouseclick(WM_LBUTTONDOWN,x,y);
					
				for(i = 0; i < 3; ++i)
				{
					if((x > mult_menu[i].pos.x) && (x < (mult_menu[i].pos.x + mult_menu[i].image.w)) && (y > mult_menu[i].pos.y) && (y < (mult_menu[i].pos.y + mult_menu[i].image.h)))
					{
						switch(i)
						{
							case 0:
								multi_player_state = WAITING;
								return MULTI;
							
							case 1:
								multi_player_state = SEARCHING;
								return MULTI;
							
							case 2:	return MENU;
						}
					}
				}
			}
		break;
		
		case WAITING:
			printObject(&background);
			printObject(&mult_menu[3]);

			iniRede();
			if(!waitClient()) return MULTI;
			else multi_player_state = SERVER;
		break;
		
		case SEARCHING:
			printObject(&background);
			printObject(&mult_menu[4]);
			
			static int try_connect = 0;
			if(try_connect) --try_connect;
			else if(!try_connect)
			{
				try_connect = 50;
				iniRede();	
				
				if(!connectToServer()) return MULTI;
				else multi_player_state = CLIENT;
			}
		break;
		
		case SERVER:
			multi_player_state = runServer();
		break;
		
		case CLIENT:
			multi_player_state = runClient();
		break;
		
		case PAUSED:
		break;
		
		case WIN:
			printObject(&background);
			printObject(&venceu);
			
			printObject(&lose_menu,true);
			
			if(ismouseclick(WM_LBUTTONDOWN))
			{
				getmouseclick(WM_LBUTTONDOWN,x,y);

				if((x > lose_menu.pos.x) && (x < (lose_menu.pos.x + lose_menu.image.w)) && (y > lose_menu.pos.y) && (y < (lose_menu.pos.y + lose_menu.image.h)))
				{
					multi_player_state = INTRO;
					return MENU;
				}
			}
		break;
		
		case LOOSE:
			printObject(&background);
			printObject(&perdeu);
			
			printObject(&lose_menu,true);
			
			if(ismouseclick(WM_LBUTTONDOWN))
			{
				getmouseclick(WM_LBUTTONDOWN,x,y);

				if((x > lose_menu.pos.x) && (x < (lose_menu.pos.x + lose_menu.image.w)) && (y > lose_menu.pos.y) && (y < (lose_menu.pos.y + lose_menu.image.h)))
				{
					multi_player_state = INTRO;
					return MENU;
				}
			}
		break;
	}
	
	
	return MULTI;
}

in_game_type runServer()
{
	
	static in_game_type single_player_state = INTRO;
	static char nivel_balde[2] = {0,0}, nivel_caixa = 0, image_player[2] = {0,0};
	static bool agua_limpa[2] = {true,true}, nuvem_sentido = true, pegou_chuva[2] = {false,false}, poop = false;
	static int count_torneira = TIME_FOSSET, count_chuva = TIME_CHUVA, count_pombo = TIME_BIRD;
	static char update_server = 0;
	static bool habilita_update = true;
	static char roda_func = 0;
	
	data_type send_server = {
		PLAYER_STATUS,
		sizeof(server_net_type),
		{}
	};
	
	// server_net_type server_data = {
		// {nuvem.pos.x,nuvem.pos.y},
		// {pombo.pos.x,pombo.pos.y},
		// {drop.pos.x,drop.pos.y},
		// {detrito.pos.x,detrito.pos.y},
		// {player_1[image_player[0]].pos.x,player_1[image_player[0]].pos.y},
		// nivel_balde[0],
		// agua_limpa[0],
		// nivel_caixa
	// };
	
	if(habilita_update)
	{
		if(update_server) --update_server;
		else
		{
			player_net_type player_data = {
				nivel_balde[0],
				agua_limpa[0],
				{player_1[image_player[0]].pos.x,player_1[image_player[0]].pos.y}
			};
			
			memcpy(&send_server.buff[0],&player_data,sizeof(player_net_type));
			
			sendData(send_server);
			update_server = 0;
			habilita_update = false;
		}
	}
	
	data_type resp;
	if(getData(resp) > 0)
	{
		if(resp.operation == PLAYER_STATUS)
		{
			nivel_balde[1] = ((player_net_type*)(&resp.buff[0]))->nivel_balde;
			agua_limpa[1] = ((player_net_type*)(&resp.buff[0]))->limpo;
			
			player_2[image_player[1]].pos.x = ((player_net_type*)(&resp.buff[0]))->local.x;
			player_2[image_player[1]].pos.y = ((player_net_type*)(&resp.buff[0]))->local.y;
		}
		else if(resp.operation == DROP_UPDATE)
		{
			if(resp.buff[0])
				count_torneira = TIME_FOSSET;
		}
		else if(resp.operation == TANK_UPDATE)
		{
			nivel_caixa = resp.buff[0];
			if(!resp.buff[1])
			{
				resetGame();
				single_player_state = INTRO;
				nivel_balde[0] = 0;
				nivel_balde[1] = 0;
				nivel_caixa = 0;
				image_player[0] = 0;
				image_player[1] = 0;
				agua_limpa[0] = true;
				agua_limpa[1] = true;
				nuvem_sentido = true;
				pegou_chuva[0] = false;
				pegou_chuva[1] = false;
				poop = false;
				count_torneira = TIME_FOSSET;
				count_chuva = TIME_CHUVA;
				count_pombo = TIME_BIRD;
				update_server = 0;
				habilita_update = true;
				roda_func = 0;
				return LOOSE;
			}
		}
	}
	
	
	if(roda_func)
	{
		--roda_func;
		return SERVER;
	}
	else
	{
		roda_func = 4;
	}
	
	// Desenha toda cena
	printObject(&scene_object);
	printObject(&building);
	printObject(&torneira);
	
	printObject(&nuvem);
	printObject(&water_tank);
	desenhaNivel(&water_tank,nivel_caixa,true);
	
	printObject(&player_2[image_player[1]]);
	desenhaNivel(&player_2[image_player[1]],nivel_balde[1],agua_limpa[1]);

	printObject(&player_1[image_player[0]]);
	desenhaNivel(&player_1[image_player[0]],nivel_balde[0],agua_limpa[0]);
	

	if((player_1[image_player[0]].pos.y+player_1[image_player[0]].image.h) >= SCREEN_H)
	{
		if(image_player[0])
		{
			image_player[0] = 0;
			
			player_1[0].pos.x = player_1[1].pos.x;
			player_1[0].pos.y = player_1[1].pos.y;
		}
		
		
		if(GetKeyState(VK_LEFT)&0x80)
		{
			player_1[image_player[0]].pos.x -= PACE - (nivel_balde[0]/10);
			habilita_update = true;
		}
		if(GetKeyState(VK_RIGHT)&0x80)
		{
			player_1[image_player[0]].pos.x += PACE - (nivel_balde[0]/10);
			habilita_update = true;
		}
	}
	else if(!image_player[0])
	{
		image_player[0] = 1;
		
		player_1[1].pos.x = player_1[0].pos.x;
		player_1[1].pos.y = player_1[0].pos.y;
	}
	
	if((player_2[image_player[1]].pos.y+player_2[image_player[1]].image.h) >= SCREEN_H)
	{
		if(image_player[1])
		{
			image_player[1] = 0;
			
			player_2[0].pos.x = player_2[1].pos.x;
			player_2[0].pos.y = player_2[1].pos.y;
		}
	}
	else if(!image_player[1])
	{
		image_player[1] = 1;
		
		player_2[1].pos.x = player_2[0].pos.x;
		player_2[1].pos.y = player_2[0].pos.y;
	}

	
	if(collidingObject(&player_1[image_player[0]],&torneira))
		if(GetKeyState('X')&0x80)
		{			
			if(!count_torneira)
			{
				
				data_type send_drop = {
					DROP_UPDATE,
					1,
					{0x01}
				};
				
				sendData(send_drop);
				
				count_torneira = TIME_FOSSET;
			}
		}
			
	if((player_1[image_player[0]].pos.x+player_1[image_player[0]].image.w) == SCREEN_W)
	{
		if(GetKeyState(VK_UP)&0x80)
		{
			player_1[image_player[0]].pos.y -= PACE - 5 - (nivel_balde[0]/10);
			habilita_update = true;
		}
		if(GetKeyState(VK_DOWN)&0x80)
		{
			player_1[image_player[0]].pos.y += PACE - 5 - (nivel_balde[0]/10);
			habilita_update = true;
		}
		
		if(nivel_balde[0])
			if((player_1[image_player[0]].pos.y+player_1[image_player[0]].image.h) == 270)
				if(GetKeyState(VK_SPACE)&0x80)
				{
					if(agua_limpa[0])
					{
						if(nivel_balde[0] > 0) nivel_balde[0] -= 10;
						else nivel_balde[0] = 0;
						
						nivel_caixa += 3;
						
						habilita_update = true;
						
						if(nivel_caixa > 100) nivel_caixa = 100;
						
						data_type send_tank = {
							TANK_UPDATE,
							2,
							{nivel_caixa,agua_limpa[0]}
						};
						
						sendData(send_tank);
					}
					else
					{
						
						data_type send_tank = {
							TANK_UPDATE,
							2,
							{nivel_caixa,agua_limpa[0]}
						};
						
						sendData(send_tank);
						
						resetGame();
						single_player_state = INTRO;
						nivel_balde[0] = 0;
						nivel_balde[1] = 0;
						nivel_caixa = 0;
						image_player[0] = 0;
						image_player[1] = 0;
						agua_limpa[0] = true;
						agua_limpa[1] = true;
						nuvem_sentido = true;
						pegou_chuva[0] = false;
						pegou_chuva[1] = false;
						poop = false;
						count_torneira = TIME_FOSSET;
						count_chuva = TIME_CHUVA;
						count_pombo = TIME_BIRD;
						update_server = 0;
						habilita_update = true;
						roda_func = 0;
						return LOOSE;
					}
				}
	}
			
			
	if(!agua_limpa[0])
	{
		if(GetKeyState(VK_SPACE)&0x80)
		{
			if(nivel_balde[0] > 0) nivel_balde[0] -= 10;
			else nivel_balde[0] = 0;
		}
		if(!nivel_balde[0]) agua_limpa[0] = true;
	}

	// Controla limites
	if(player_1[image_player[0]].pos.x < 0) player_1[image_player[0]].pos.x = 0;
	if((player_1[image_player[0]].pos.x+player_1[image_player[0]].image.w) >= SCREEN_W) player_1[image_player[0]].pos.x = SCREEN_W-player_1[image_player[0]].image.w;
	if((player_1[image_player[0]].pos.y+player_1[image_player[0]].image.h) >= SCREEN_H) player_1[image_player[0]].pos.y = SCREEN_H-player_1[image_player[0]].image.h;
	if((player_1[image_player[0]].pos.y+player_1[image_player[0]].image.h) <= 270) player_1[image_player[0]].pos.y = 270-player_1[image_player[0]].image.h;
			
	if(nuvem_sentido)
		nuvem.pos.x += PACE;
	else
		nuvem.pos.x -= PACE;

	data_type send_nuvem = {
		CLOUD_UPDATE,
		sizeof(position_type),
		{0}
	};
			
	memcpy(&send_nuvem.buff[0],&nuvem.pos,sizeof(position_type));
	sendData(send_nuvem);
	
	if(nuvem.pos.x < 0)
	{
		nuvem.pos.x = 0;
		nuvem_sentido = true;
	}
			
	if((nuvem.pos.x+nuvem.image.w) >= SCREEN_W)
	{
		nuvem.pos.x = SCREEN_W-nuvem.image.w;
		nuvem_sentido = false;
	}
			
	if(count_pombo) --count_pombo;
			
	if(!count_pombo)
	{
		// if(!pombo.pos.x)
		// {
			data_type send_pombo = {
				BIRD_UPDATE,
				sizeof(position_type),
				{0}
			};
					
			memcpy(&send_pombo.buff[0],&pombo.pos,sizeof(position_type));
			sendData(send_pombo);
		// }
	
		printObject(&pombo);
		
		if(!poop)
			if(collidingObject(&pombo,&nuvem))
			{
				// data_type send_poop = {
					// SHIT_UPDATE,
					// 1,
					// {1}
				// };
				
				// sendData(send_poop);
				
				detrito.pos.x = pombo.pos.x;
				poop = true;
			}

		pombo.pos.x += PACE;

		if(pombo.pos.x >= SCREEN_W)
		{
			// data_type send_pombo = {
				// BIRD_UPDATE,
				// 1,
				// {1}
			// };
					
			// sendData(send_pombo);
			
			pombo.pos.x = 0;
			count_pombo = TIME_BIRD;
		}
	}
			
	if(poop)
	{
		
		data_type send_poop = {
			SHIT_UPDATE,
			sizeof(position_type),
			{0}
		};
		
		memcpy(&send_poop.buff[0],&detrito.pos,sizeof(position_type));
		
		sendData(send_poop);

		printObject(&detrito);
		
		if(collidingObject(&detrito,&player_1[image_player[0]])) agua_limpa[0] = false;
			
		if(detrito.pos.y >= SCREEN_H)
		{
			detrito.pos.y = shit_start;
			poop = false;
		}
		else
			detrito.pos.y += 20;
	}
			
	if(nivel_caixa)
		if(count_torneira)
			--count_torneira;
			
	if(!count_torneira)
	{
		data_type send_drop = {
			DROP_UPDATE,
			1,
			{0}
		};
		
		sendData(send_drop);
		
		if(nivel_caixa)
		{
			printObject(&drop);

			if(drop.pos.y+drop.image.h >= SCREEN_H)
			{
				drop.pos.y = drop_start;
				nivel_caixa -= 1;
			}
			else
				drop.pos.y += 5;
		}
	}
			
	if(count_chuva)
	{
		--count_chuva;
		chuva.pos.x = nuvem.pos.x;
	}
	else
	{
		if(!pegou_chuva[0])
			if(collidingObject(&player_1[image_player[0]],&chuva))
			{
				if(nivel_balde[0] < 100) nivel_balde[0] += 25;
				else nivel_balde[0] = 100;
				pegou_chuva[0] = true;
			}

		if((chuva.pos.y >= SCREEN_H))
		{	
			pegou_chuva[0] = false;
			
			data_type send_chuva = {
				RAIN_UPDATE,
				sizeof(position_type),
				{0}
			};
					
			memcpy(&send_chuva.buff[0],&chuva.pos,sizeof(position_type));
			sendData(send_chuva);
			
			chuva.pos.y = rain_start;
			count_chuva = TIME_CHUVA;
		}
		else
		{
	
			data_type send_chuva = {
				RAIN_UPDATE,
				sizeof(position_type),
				{0}
			};
					
			memcpy(&send_chuva.buff[0],&chuva.pos,sizeof(position_type));
			sendData(send_chuva);
			
			printObject(&chuva);
			chuva.pos.y += 10;
			
		}
			
		
	}
			
	if(GetKeyState(VK_ESCAPE)&0x80)
	{
		//intro = SCREEN_W/2;
		//return MENU;
		single_player_state = PAUSED;
	}
	
	if(nivel_caixa == 100)
	{
		resetGame();
		single_player_state = INTRO;
		nivel_balde[0] = 0;
		nivel_balde[1] = 0;
		nivel_caixa = 0;
		image_player[0] = 0;
		image_player[1] = 0;
		agua_limpa[0] = true;
		agua_limpa[1] = true;
		nuvem_sentido = true;
		pegou_chuva[0] = false;
		pegou_chuva[1] = false;
		poop = false;
		count_torneira = TIME_FOSSET;
		count_chuva = TIME_CHUVA;
		count_pombo = TIME_BIRD;
		update_server = 0;
		habilita_update = true;
		roda_func = 0;
		return WIN;
	}

	if(contador())
	{
		
		resetGame();
		single_player_state = INTRO;
		nivel_balde[0] = 0;
		nivel_balde[1] = 0;
		nivel_caixa = 0;
		image_player[0] = 0;
		image_player[1] = 0;
		agua_limpa[0] = true;
		agua_limpa[1] = true;
		nuvem_sentido = true;
		pegou_chuva[0] = false;
		pegou_chuva[1] = false;
		poop = false;
		count_torneira = TIME_FOSSET;
		count_chuva = TIME_CHUVA;
		count_pombo = TIME_BIRD;
		update_server = 0;
		habilita_update = true;
		roda_func = 0;
		return LOOSE;
	}

	return SERVER;
}

in_game_type runClient()
{
	static in_game_type single_player_state = INTRO;
	static char nivel_balde[2] = {0,0}, nivel_caixa = 0, image_player[2] = {0,0};
	static bool agua_limpa[2] = {true,true}, nuvem_sentido = true, pegou_chuva[2] = {false,false}, poop = false;
	static int count_torneira = TIME_FOSSET, count_chuva = TIME_CHUVA, count_pombo = TIME_BIRD;
	static char update_server = 0;
	static bool habilita_update = true;
	static char roda_func = 0;

	if(habilita_update)
	{
		if(update_server) --update_server;
		else
		{
			data_type send_server = {
				PLAYER_STATUS,
				sizeof(player_net_type),
				{}
			};
			
			player_net_type client_data = {
				nivel_balde[1],
				agua_limpa[1],
				{player_2[image_player[1]].pos.x,player_2[image_player[1]].pos.y}
			};
			
			memcpy(&send_server.buff[0],&client_data,sizeof(player_net_type));
			
			sendData(send_server);
			update_server = 0;
			habilita_update = false;
		}
	}		
	
	static char tempo[20];
	
	data_type resp;
	if(getData(resp) > 0)
	{
		if(resp.operation == PLAYER_STATUS)
		{
			nivel_balde[0] = ((player_net_type*)(&resp.buff[0]))->nivel_balde;
			agua_limpa[0] = ((player_net_type*)(&resp.buff[0]))->limpo;
			
			player_1[image_player[0]].pos.x = ((player_net_type*)(&resp.buff[0]))->local.x;
			player_1[image_player[0]].pos.y = ((player_net_type*)(&resp.buff[0]))->local.y;
		}
		else if(resp.operation == DROP_UPDATE)
		{
			count_torneira = resp.buff[0];
		}
		else if(resp.operation == TANK_UPDATE)
		{
			nivel_caixa = resp.buff[0];
			
			if(!resp.buff[1])
			{
				
				resetGame();
				single_player_state = INTRO;
				nivel_balde[0] = 0;
				nivel_balde[1] = 0;
				nivel_caixa = 0;
				image_player[0] = 0;
				image_player[1] = 0;
				agua_limpa[0] = true;
				agua_limpa[1] = true;
				nuvem_sentido = true;
				pegou_chuva[0] = false;
				pegou_chuva[1] = false;
				poop = false;
				count_torneira = TIME_FOSSET;
				count_chuva = TIME_CHUVA;
				count_pombo = TIME_BIRD;
				update_server = 0;
				habilita_update = true;
				roda_func = 0;
				return LOOSE;
			}
			
			if(nivel_caixa == 100)
			{
				resetGame();
				single_player_state = INTRO;
				nivel_balde[0] = 0;
				nivel_balde[1] = 0;
				nivel_caixa = 0;
				image_player[0] = 0;
				image_player[1] = 0;
				agua_limpa[0] = true;
				agua_limpa[1] = true;
				nuvem_sentido = true;
				pegou_chuva[0] = false;
				pegou_chuva[1] = false;
				poop = false;
				count_torneira = TIME_FOSSET;
				count_chuva = TIME_CHUVA;
				count_pombo = TIME_BIRD;
				update_server = 0;
				habilita_update = true;
				roda_func = 0;
				return WIN;
			}
		}
		else if(resp.operation == RAIN_UPDATE)
		{
			chuva.pos.x = ((position_type*)(&resp.buff[0]))->x;
			chuva.pos.y = ((position_type*)(&resp.buff[0]))->y;
			
			// printObject(&chuva);
			
			count_chuva = 0;
		}
		else if(resp.operation == CLOUD_UPDATE)
		{
			nuvem.pos.x = ((position_type*)(&resp.buff[0]))->x;
			nuvem.pos.y = ((position_type*)(&resp.buff[0]))->y;
		}
		else if(resp.operation == BIRD_UPDATE)
		{
			pombo.pos.x = ((position_type*)(&resp.buff[0]))->x;
			pombo.pos.y = ((position_type*)(&resp.buff[0]))->y;
			
			count_pombo = 0;
			
		}
		else if(resp.operation == SHIT_UPDATE)
		{
			detrito.pos.x = ((position_type*)(&resp.buff[0]))->x;
			detrito.pos.y = ((position_type*)(&resp.buff[0]))->y;
			
			poop = true;
			// printObject(&detrito);
		}
		else if(resp.operation == TIME_UPDATE)
		{
			sprintf(tempo,"Tempo: %d",resp.buff[0]);
			
			if(!resp.buff[0])
			{
				resetGame();
				single_player_state = INTRO;
				nivel_balde[0] = 0;
				nivel_balde[1] = 0;
				nivel_caixa = 0;
				image_player[0] = 0;
				image_player[1] = 0;
				agua_limpa[0] = true;
				agua_limpa[1] = true;
				nuvem_sentido = true;
				pegou_chuva[0] = false;
				pegou_chuva[1] = false;
				poop = false;
				count_torneira = TIME_FOSSET;
				count_chuva = TIME_CHUVA;
				count_pombo = TIME_BIRD;
				update_server = 0;
				habilita_update = true;
				roda_func = 0;
				return LOOSE;
			}
		}
		//nivel_caixa = ((server_net_type*)(&resp.buff[0]))->nivel_caixa;
		
		// nuvem.pos.x = ((server_net_type*)(&resp.buff[0]))->nuvem.x;
		// nuvem.pos.y = ((server_net_type*)(&resp.buff[0]))->nuvem.y;
	}
	
	if(roda_func)
	{
		--roda_func;
		return CLIENT;
	}
	else
	{
		roda_func = 4;
	}
	
	// Desenha toda cena
	printObject(&scene_object);
	printObject(&building);
	printObject(&torneira);
	
	printObject(&nuvem);
	printObject(&water_tank);
	desenhaNivel(&water_tank,nivel_caixa,true);
	
	printObject(&player_2[image_player[1]]);
	desenhaNivel(&player_2[image_player[1]],nivel_balde[1],agua_limpa[1]);

	printObject(&player_1[image_player[0]]);
	desenhaNivel(&player_1[image_player[0]],nivel_balde[0],agua_limpa[0]);
	
	setbkcolor(COLOR(37,162,222));
	settextstyle(4,HORIZ_DIR, 3);
	outtextxy(0,0,tempo);
	
	if((player_2[image_player[1]].pos.y+player_2[image_player[1]].image.h) >= SCREEN_H)
	{
		if(image_player[1])
		{
			image_player[1] = 0;
			
			player_2[0].pos.x = player_2[1].pos.x;
			player_2[0].pos.y = player_2[1].pos.y;
		}
		
		
		if(GetKeyState(VK_LEFT)&0x80)
		{
			player_2[image_player[1]].pos.x -= PACE - (nivel_balde[1]/10);
			habilita_update = true;// update_server = 1;
		}
		if(GetKeyState(VK_RIGHT)&0x80)
		{
			player_2[image_player[1]].pos.x += PACE - (nivel_balde[1]/10);
			habilita_update = true;// update_server = 1;
		}
	}
	else if(!image_player[1])
	{
		image_player[1] = 1;
		
		player_2[1].pos.x = player_2[0].pos.x;
		player_2[1].pos.y = player_2[0].pos.y;
	}
	
	if((player_1[image_player[0]].pos.y+player_1[image_player[0]].image.h) >= SCREEN_H)
	{
		if(image_player[0])
		{
			image_player[0] = 0;
			
			player_1[0].pos.x = player_1[1].pos.x;
			player_1[0].pos.y = player_1[1].pos.y;
		}
	}
	else if(!image_player[0])
	{
		image_player[0] = 1;
		
		player_1[1].pos.x = player_1[0].pos.x;
		player_1[1].pos.y = player_1[0].pos.y;
	}

	if((player_2[image_player[1]].pos.x+player_2[image_player[1]].image.w) == SCREEN_W)
	{
		if(GetKeyState(VK_UP)&0x80)
		{
			player_2[image_player[1]].pos.y -= PACE - 5 - (nivel_balde[1]/10);
			habilita_update = true;
		}
		if(GetKeyState(VK_DOWN)&0x80)
		{
			player_2[image_player[1]].pos.y += PACE - 5 - (nivel_balde[1]/10);
			habilita_update = true;
		}
		
		if(nivel_balde[1])
			if((player_2[image_player[1]].pos.y+player_2[image_player[1]].image.h) == 270)
				if(GetKeyState(VK_SPACE)&0x80)
				{
					if(agua_limpa[1])
					{
						if(nivel_balde[1] > 0) nivel_balde[1] -= 10;
						else nivel_balde[1] = 0;
						
						nivel_caixa += 3;
						
						habilita_update = true;
						
						if(nivel_caixa > 100) nivel_caixa = 100;
						
						data_type send_tank = {
							TANK_UPDATE,
							2,
							{nivel_caixa,agua_limpa[0]}
						};
						
						sendData(send_tank);
					}
					else
					{
						data_type send_tank = {
							TANK_UPDATE,
							2,
							{nivel_caixa,agua_limpa[1]}
						};
						
						sendData(send_tank);
						
						resetGame();
						single_player_state = INTRO;
						nivel_balde[0] = 0;
						nivel_balde[1] = 0;
						nivel_caixa = 0;
						image_player[0] = 0;
						image_player[1] = 0;
						agua_limpa[0] = true;
						agua_limpa[1] = true;
						nuvem_sentido = true;
						pegou_chuva[0] = false;
						pegou_chuva[1] = false;
						poop = false;
						count_torneira = TIME_FOSSET;
						count_chuva = TIME_CHUVA;
						count_pombo = TIME_BIRD;
						update_server = 0;
						habilita_update = true;
						roda_func = 0;
						return LOOSE;
					}
				}
	}	
			
	if(!agua_limpa[1])
	{
		if(GetKeyState(VK_SPACE)&0x80)
		{
			if(nivel_balde[1] > 0) nivel_balde[1] -= 10;
			else nivel_balde[1] = 0;
		}
		if(!nivel_balde[1]) agua_limpa[1] = true;
	}

	
	// Controla limites
	if(player_2[image_player[1]].pos.x < 0) player_2[image_player[1]].pos.x = 0;
	if((player_2[image_player[1]].pos.x+player_2[image_player[1]].image.w) >= SCREEN_W) player_2[image_player[1]].pos.x = SCREEN_W-player_2[image_player[1]].image.w;
	if((player_2[image_player[1]].pos.y+player_2[image_player[1]].image.h) >= SCREEN_H) player_2[image_player[1]].pos.y = SCREEN_H-player_2[image_player[1]].image.h;
	if((player_2[image_player[1]].pos.y+player_2[image_player[1]].image.h) <= 270) player_2[image_player[1]].pos.y = 270-player_2[image_player[1]].image.h;
	
	// if(nuvem_sentido)
		// nuvem.pos.x += PACE;
	// else
		// nuvem.pos.x -= PACE;

	// if(nuvem.pos.x < 0)
	// {
		// nuvem.pos.x = 0;
		// nuvem_sentido = true;
	// }
			
	// if((nuvem.pos.x+nuvem.image.w) >= SCREEN_W)
	// {
		// nuvem.pos.x = SCREEN_W-nuvem.image.w;
		// nuvem_sentido = false;
	// }
	
	if(collidingObject(&player_2[image_player[1]],&torneira))
		if(GetKeyState('X')&0x80)
		{			
			if(!count_torneira)
			{
				
				data_type send_drop = {
					DROP_UPDATE,
					1,
					{0x01}
				};
				
				sendData(send_drop);
				
				count_torneira = TIME_FOSSET;
			}
		}
	
	if(!count_torneira)
	{		
		if(nivel_caixa)
		{
			printObject(&drop);

			if(drop.pos.y+drop.image.h >= SCREEN_H)
			{
				drop.pos.y = drop_start;
				nivel_caixa -= 1;
			}
			else
				drop.pos.y += 5;
		}
	}
	
	if(!count_chuva)
	{
		if(!pegou_chuva[1])
			if(collidingObject(&player_2[image_player[1]],&chuva))
			{
				pegou_chuva[1] = true;
				
				if(nivel_balde[1] < 100) nivel_balde[1] += 25;
				else nivel_balde[1] = 100;
				
				habilita_update = true;
			}

		if((chuva.pos.y >= SCREEN_H))
		{
			pegou_chuva[1] = false;
			// chuva.pos.y = rain_start;
			count_chuva = 1;
		}
		else
		{
			printObject(&chuva);
			// chuva.pos.y += 10;					
		}
	}
	
	if(!count_pombo)
	{
		printObject(&pombo);

		// pombo.pos.x += PACE;

		if(pombo.pos.x >= SCREEN_W)
		{
			// pombo.pos.x = 0;
			count_pombo = 1;
		}
	}

	if(poop)
	{
		printObject(&detrito);
		
		if(collidingObject(&detrito,&player_2[image_player[1]])) 
		{
			agua_limpa[1] = false;
			habilita_update = true;
		}
			
		if(detrito.pos.y >= SCREEN_H)
		{
			poop = false;
			detrito.pos.y = shit_start;
		}
		// else
			// detrito.pos.y += 20;
	}
	
	if(nivel_caixa == 100)
	{
		
		resetGame();
		single_player_state = INTRO;
		nivel_balde[0] = 0;
		nivel_balde[1] = 0;
		nivel_caixa = 0;
		image_player[0] = 0;
		image_player[1] = 0;
		agua_limpa[0] = true;
		agua_limpa[1] = true;
		nuvem_sentido = true;
		pegou_chuva[0] = false;
		pegou_chuva[1] = false;
		poop = false;
		count_torneira = TIME_FOSSET;
		count_chuva = TIME_CHUVA;
		count_pombo = TIME_BIRD;
		update_server = 0;
		habilita_update = true;
		roda_func = 0;
		return WIN;
	}
	
	return CLIENT;
}

bool contador()
{
	char tempo[20];
	
	if(contador_value) --contador_value;
	
	setbkcolor(COLOR(37,162,222));
	settextstyle(4,HORIZ_DIR, 3);
	sprintf(tempo,"Tempo: %d",contador_value/40);
	
	data_type send_time = {
		TIME_UPDATE,
		sizeof(contador_value),
		{contador_value/40}
	};
		
	sendData(send_time);
	
	outtextxy(0,0,tempo);

	if(!contador_value) return true;
	else return false;
}

void resetGame()
{
	water_tank.pos.y = SCREEN_H-game_images[WATER_TANK].h-game_images[BUILDING].h-40;
	water_tank.pos.x = SCREEN_W-game_images[WATER_TANK].w;
	
	torneira.pos.y = SCREEN_H-180;
	torneira.pos.x = SCREEN_W-game_images[BUILDING].w;
	
	drop.pos.y = drop_start = torneira.pos.y+game_images[DROP].h+20;
	drop.pos.x = torneira.pos.x;
	
	nuvem.pos.y = 40;
	nuvem.pos.x = 0;
	
	pombo.pos.y = 40;
	pombo.pos.x = 0;
	
	detrito.pos.y = shit_start = pombo.pos.y+game_images[SHIT].h;
	detrito.pos.x = 0;
	
	chuva.pos.y = rain_start = nuvem.pos.y+game_images[WATER].h;
	chuva.pos.x = 0;
	
	player_1[0].pos.x = 0;
	player_1[0].pos.y = SCREEN_H-player_1[0].image.h;
	
	player_2[0].pos.x = 0;
	player_2[0].pos.y = SCREEN_H-player_2[0].image.h;
	
	contador_value = VALUE_COUNT;
}

void buckyDance(void)
{
	char i,j;
	
	for(i = 2; i < 16; ++i)
	{
		player_1[i].pos.x = player_1[1].pos.x;
		player_1[i].pos.y = player_1[1].pos.y;
	}
	
	for(j = 0; j < 5; ++j)
	{
		for(i = 2; i < 16; ++i)
		{
			printObject(&scene_object);
			printObject(&building);
			printObject(&torneira);
			printObject(&nuvem);
			printObject(&water_tank);
			printObject(&player_1[i]);
			updateScreen();
			delay(20);
		}
	}

}