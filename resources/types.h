/**
 *  @file types.h
 *  @brief Arquivo contendo as defini��es comuns aos demais m�dulos
 *  do projeto
 */

#ifndef __TYPES_H__
#define __TYPES_H__

#include <math.h>
#include <iostream>

// Idiomas disponiveis no jogo
typedef enum
{
	PORT,
	ING,
	QTD_IDIOMA
}idioma_type;

// Estrutura que guarda informacoes das imagens
typedef struct{
	void *img; /**< Imagem carregada */
	void *msk; /**< Mascara da imagem */
	float h; /**< Altura da imagem */
	float w; /**< Largadura da imagem */
}img_data_type;

// Posicao na tela
typedef struct{
	int x;
	int y;
}position_type;

typedef struct{
	img_data_type image;
	position_type pos;
}object_data_type;

// Enum que define o estado que o jogo vai estar
typedef enum
{
	INIT, /**< Inicializacao do jogo */
	MENU, /**< Menu com todas opcoes */
	CONFIG, /**< Configuracao de alguma coisa */
	SINGLE, /**< Jogo single player */
	MULTI, /**< Jogo multi player */
	CREDITS, /**< Credito dos criadores */
	EXIT	/**< Sai do jogo */
}game_state_type;

// Enum com estados poss�veis dentro do jogo
typedef enum{
	INTRO,
	PLAYING,
	WAITING,
	SEARCHING,
	SERVER,
	CLIENT,
	PAUSED,
	ENDING,
	WIN,
	LOOSE
}in_game_type;

// Server
typedef struct{
	position_type nuvem;   //.
	position_type passaro; //.   Movimento espont�neo
	position_type gota;    //.
	position_type detrito; //.
	
	position_type balde;   // Controlado pelo usuario
	int nivel_balde;   // Controlado pelo usuario
	bool limpo;
	
	int nivel_caixa;
}server_net_type;

// Client
typedef struct{
	int nivel_balde;   // Controlado pelo usuario
	bool limpo;
    position_type local;
}player_net_type;

// Objetos graficos que o jogo vai ter
enum
{
	SINGLEPLAYER = 0,
	MULTIPLAYER,
	SETTINGS,
	QUIT,

	LANGUAGE,
	BACK,
	
	PAUSE,
	RESUME_GAME,
	QUIT_GAME,
	
	CREATE,
	CONNECT,
	
	WAITING_GAME,
	SEARCHING_GAME,
	
	BALAO1,
	
	PLAYER1,
	PLAYER1_COSTAS,
	
	DANCE0,
	DANCE1,
	DANCE2,
	DANCE3,
	DANCE4,
	DANCE5,
	DANCE6,
	DANCE7,
	DANCE8,
	DANCE9,
	DANCE10,
	DANCE11,
	DANCE12,
	DANCE13,
	
	PLAYER2,
	PLAYER2_COSTAS,
	
	BUILDING,
	WATER_TANK,

	WATER,
	BIRD,
	SHIT,
	
	CLOUD,
	FOSSET,
	DROP,
	
	GANHOU,
	PERDEU,

	QTD_GAME_OBJ
	
};

#define QTD_BALOES		1
#define QTD_MAIN_MENU	4
#define QTD_CONFIG_MENU	2

#endif
